module TimeMethodsForInteger

  WEEK_DAYS_VALUE = { monday:    1,
                      tuesday:   2,
                      wednesday: 3,
                      thursday:  4,
                      friday:    5,
                      saturday:  6,
                      sunday:    7
                    }

  def business_days_from_now

    raise ArgumentError, 'The receiver should be a natural number' unless self > 0

    business_days_generator =  Enumerator.new do |y|
      date = Date.today
      loop do
        date = date.next
        next if (date.sunday? || date.saturday?)
        y.yield date
      end
    end

    business_days_generator.first(self).last.to_datetime

  end

  def week(day)

    raise ArgumentError, 'The receiver should be a whole number' unless self >= 0
    raise ArgumentError, 'The passed argument does not represent a valid day.' unless WEEK_DAYS_VALUE.include? day

    today = Date.today
    today_cwday = today.cwday
    target_cwday = WEEK_DAYS_VALUE[day]

    if self == 0
      raise ArgumentError, 'The day has already passed for this week.' if today_cwday > target_cwday
      if today_cwday == target_cwday
        return today
      else
        return today + (target_cwday - today_cwday)
      end
    else
      days_remaining_in_this_week = 7 - today_cwday
      days_to_be_added = days_remaining_in_this_week + ((self - 1) * 7) + target_cwday
      return today + days_to_be_added
    end

  end

end